# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

require 'settings'

# Spits out clean error messages (no stack traces) and exits with error.
class Error
  # Throw the error message.  Sender is the name of the class that
  # threw the message, message explains what the error is.  If the quiet
  # setting is enabled, don't print the error message and if the force
  # setting is enabled, don't exit.
  def self.throw(sender, message)
    if !Settings::settings[:quiet]
      puts "Error in #{sender}: #{message}"
    end
    if !Settings::settings[:force]
      exit 1
    end
  end
end
