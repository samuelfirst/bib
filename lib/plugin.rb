# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

require 'psych'
require 'error'
require 'settings'

# Class to load plugins for scraping citation info from various
# sources.  It can load either ruby plugins natively, or plugins
# in other languages via pipes.  If it's loading a plugin via
# pipe, it expects that plugin to communicate via yaml (or json,
# since yaml parsers can parse it).  If it's loading the plugin
# natively, it expects the plugin to provide an execution point
# in the manifest.
class Plugin
  # Start by loading the manifest file, then perform sanity checks
  # on the manifest, then try to load the plugin
  def self.load(plugin_name, string)
    # Load the manifest file
    manifest = Psych::load(Settings::settings[:manifest])

    # Sanity check the manifest
    if !manifest[plugin_name]
      Error::throw("Plugin", "Unknown plugin #{plugin_name}")
    elsif !manifest[plugin_name]["type"]
      Error::throw("Plugin", "No type specified for #{plugin_name}")
    elsif !['native', 'pipe'].include? manifest[plugin_name]["type"]
      Error::throw("Plugin", "Invalid type specified for #{plugin_name}")
    elsif !manifest[plugin_name]["path"]
      Error::throw("Plugin", "No path specified for #{plugin_name}")
    elsif !manifest[plugin_name]["exec_point"]
      Error::throw("Plugin", "No exec point specified for #{plugin_name}")
    end
    
    # If it's a native plugin, require it, then run it via the
    # execution point.
    if manifest[plugin_name]["type"] == "native"
      for file in manifest[plugin_name]["path"]
        begin
          require file
        rescue LoadError
          next
        end
      end

      # Run plugin from execution point, substituting STRING for the data
      # being passed.
      begin
        exec_point = manifest[plugin_name]['exec_point']
        exec_point.sub!('STRING', string)
        info = eval "#{exec_point}"
      rescue NameError
        Error::throw("Plugin", "Unknown method, or wrong path")
      end

    # If it's a piped plugin, substitute PATH in the exec point for
    # the path and STRING for the data being passed, then run the plugin.
    # Finally, deserialize the output.
    elsif manifest[plugin_name]["type"] == "pipe"
      exec_point = manifest[plugin_name]['exec_point']
      exec_point.sub!('PATH', manifest[plugin_name]['path'])
      exec_point.sub!('STRING', string)
      info = eval "`#{exec_point}`"
      info = Psych::load(info)
    end

    return info
  end
end
