# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

# This class contains functions to convert an html document into
# a hash containing a series of arrays.  There is no logic to
# bail on badly formed html, so it is possible that in those
# cases that it will produce odd results.  This is done intentionally,
# because the documents are only needed for parsing, so it doesn't
# matter if parts of them are broken.  It also does not maintain the
# structure of the document, since it isn't needed for the purposes of
# this program, and is easier to just discard.
#
# The conversion should go something like this:
#    +-------------------------------------------------------+
#    |  <html xmlns:og="http://ogp.me/ns#">                  |
#    |    <head>                                             |
#    |      <title>Title</title>                             |
#    |    </head>                                            |
#    |    <body>                                             |
#    |      <p>Paragraph 1</p>                               |
#    |      <p>Paragraph 2</p>                               |
#    |    </body>                                            |
#    |  </html>                                              |
#    +--------------------------[TO]-------------------------+  
#    |  {"html" => [["xmlns:og=\"http://ogp.me/ns#\"", ""]]  |
#    |   "head" => [["", ""]]                                |
#    |   "body" => [["", ""]]                                |
#    |      "p" => [["", "Paragraph 1"],                     |
#    |              ["", "Paragraph 2"]]}                    |
#    +-------------------------------------------------------+
#
# This means that the paragraph elements in the body could be accessed
# like this:
#    unmarshalled_doc["p"].each {|p| puts p[1]}
#    #=> Paragraph 1
#        Paragraph 2
# 
# The first item in each array is anything contained inside the opening
# tag, the second item is everything between the opening and closing tags.
class HTML
  # Convert the document from a string to nested hashes and arrays
  # (kind of like json).
  #
  # We start by splitting the string into an array on new lines.
  # This will make it slightly easier to manipulate as we go through
  # it.
  #
  # We can safely ignore the document type, since it isn't really
  # important for our purposes.  The easiest way to do this is to just
  # discard the first line if it matches `<DOCTYPE! html>`.
  #
  # Then we run through each line of the document, pushing each tag and
  # it's contents into a hash.
  def unmarshall(document)
    # Do a sanity check to make sure the document is a string.
    if document.class != String
      raise ArgumentError, 'Invalid Argument: document must be a string'
    end

    # Convert the document into an array by splitting on new lines
    document = document.split("\n")

    # If the first line is a document type declaration, discard it.
    if document[0] == "<!DOCTYPE html>"
      document = document[1..]
    end

    # Run through each line of the document, and for each opened tag
    # stick it's data in an array.  Then stick the array in the tag's
    # assosciated hash.
    doc = {}                                # Hash containing deserialized doc
    stack = []                              # Contains unclosed tags
    in_string = ""                          # Contains open string quote
    in_comment = false                      # True if in multiline comment
    bracket_open = false                    # True if inside opening tag
    need_tag_name = true                    # True if tag name still needed
    interpret_literally = false             # True if char is escaped
    tag = {"name" => '', "data" => ['','']} # Contains tag name/data
    tag_closed = true                       # True if tag is closed
    in_closing_tag = false                  # True when in closing tag
    closing_tag_name = ""                   # Holds the name of closing tag
    document.each do |line|
      
      prev_char = ''                        # Previous character
      line.each_char do |char|
        # If the previous char was an escape character, interpret
        # the current character literally
        if interpret_literally or !in_string.empty?
          if need_tag_name
            tag << char
          elsif in_closing_tag
            closing_tag_name << char
          elsif bracket_open
            tag_data[0] << char
          else
            tag_data[1] << char
          end
          interpret_literally = false
          
        # If the char is an escape character, set interpret_literally
        # to true.
        elsif char == "\\"
          interpret_literally = true
          
        # If the char is a string char, toggle in_string open (" or ')
        # or closed (an empty string).
        elsif char == '"' or char == "'"
          if !in_string.empty? and char == in_string
            in_string = ""
          else
            in_string = char
          end
          
        # If the char is an opening bracket, set bracket_open to true
        elsif char == "<"
          bracket_open = true
          need_tag_name = true
          
        # If the char is a closing bracket, set bracket_open to false,
        # unless it's a closing tag, in which case we check it against
        # the current tag name and if they match push it to doc.  If
        # they don't match, we pop from the stack until they do, then
        # rebuild the stack after.
        elsif char == ">"
          if in_closing_tag
            
          else
            bracket_open = false
          end

        # If the char is a forward slash and the previous character was
        # an opening angle bracket, we know that we're at the beginning
        # of a closing tag.
        elsif char == "/" and prev_char == "<"
          in_closing_tag = true

        # If the char is a space and the name of the current tag is still
        # unknown, we know we've reached the end of the name of the tag.
        # Otherwise, it's just a normal space, so we append it to the
        # appropriate place.
        elsif char == " "
          if need_tag_name
            need_tag_name = false
          else
            if bracket_open
              tag_data[0] << char
            else
              tag_data[1] << char
            end
          end

        # Just a normal character; append it to the appropriate place.
        else
          if need_tag_name
            tag << char
          elsif in_closing_tag
            closing_tag_name << char
          elsif bracket_open
            tag_data[0] << char
          else
            tag_data[1] << char
          end
        end

        # Set the previous character to the current character.
        prev_char = char
      end
    end

    # Return the unmarshalled document
    return doc
  end
end
