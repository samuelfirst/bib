# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

require 'net/http'
require 'json'

# This class pulls bibliography information from openlibrary.org
# using their RESTful API.
# * Open Library Project: https://openlibrary.org
# * Documentation on the API: https://openlibrary.org/developers/api
# * Open Library Source: https://github.com/internetarchive/openlibrary
class OpenLibrary
  # Returns the query (string) as a URI object.
  # The URI will point to openlibrary.org/search.json?=<query>
  #
  # Example:
  #   my_url = OpenLibrary::build_query('the art of unix programming')
  def self.build_query(query)
    if query.class != String
      raise ArgumentError, 'Invalid Argument: must be a string'
    end
    return URI("http://openlibrary.org/search.json?q=#{query.gsub(' ', '+')}")
  end

  # Returns a hash with the following keys:
  # * start: integer, starting index
  # * num_found/numFound: integer, total number of results
  # * docs: array of hashes
  #   * hashes contain the following keys:
  #     * title_suggest: string, suggestion for title
  #     * edition_key: array of strings, open library edition identifier
  #     * cover_i: integer
  #     * isbn: integer, isbn number
  #     * has_fulltext: boolean
  #     * text: array of strings, condensed version of info:
  #       * edition key
  #       * isbn(s)
  #       * Author
  #       * ia
  #       * author key
  #       * subject(s)
  #       * file type
  #       * Title
  #       * Seed
  #       * Author full name
  #       * Publisher
  #       * lccn
  #     * author_name: array of strings, name(s) of author(s)
  #     * seed: array of strings, subpages containing the work
  #     * ia: array of strings
  #     * author_key: array of strings, open library author identifier
  #     * subject: array of strings, subject(s) of the book
  #     * title: string, title of the book
  #     * ia_collection_s: string
  #     * printdisabled_s: string
  #     * type: string, type of book
  #     * ebook_count_i: integer
  #     * publish_place: array of strings, place(s) book was published
  #     * edition_count: integer
  #     * key: string, subpage containing the work
  #     * id_goodreads: array of strings, goodreads identifier
  #     * public_scan_b: boolean
  #     * publisher: array of strings, publisher(s) of work
  #     * language: array of strings, language codes work was published in
  #     * lccn: array of strings, library of congress control number
  #     * last_modified_i: integer
  #     * first_publish_year: integer, first year the work was published
  #     * id_librarything: array of strings
  #     * cover_edition_key: string
  #     * publish_year: array of integers, year(s) published
  #     * publish_date: array of strings, date(s) published
  #   * Some of these do not have descriptions.  This is because I can not
  #     find any documentation on what they are.
  # Example:
  #   author = OpenLibrary::query_library(my_url)['docs'][0]['author_name']
  def self.query_library(url)
    query_info = Net::HTTP.get(url)
    return JSON::parse(query_info)
  end
end
