# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

require 'psych'
require 'optparse'

# This class holds the settings for the program.  The settings are loaded
# from the following sources: the default settings defined in this file,
# settings loaded from the run control file (formatted as either yaml or
# json), and settings defined with command-line args, loaded in that order.
class Settings
  @settings = {:quiet => false,
               :force => false,
               :columns => 80,
               :tab => 4,
               :rc => File::expand_path("~/.config/bibrc"),
               :load_secondary_rc => false,
               :manifest =>
               File::expand_path("~/.local/share/bib/manifest.yaml")}
  def self.settings
    @settings
  end

  # Parse settings from command line flags.
  # 
  # Flags are:
  # * -q, --quiet    :: Suppress error messages
  # * -f, --force    :: Ignore errors
  # * -c, --columns  :: Number of characters to wrap line at
  # * -t, --tab      :: Number of spaces to use for a tab
  # * -l, --load     :: Load a secondary run control file
  # * -m, --manifest :: Load alternate manifest file
  def self.parse_args
    OptionParser.new do |opts|
      opts.banner = "Usage: bib [options]"
      opts.on("-q", "--quiet", "Suppress Error Messages") do |bool|
        @settings[:quiet] = bool
      end
      opts.on("-f", "--force", "Ignore Errors") do |bool|
        @settings[:force] = bool
      end
      opts.on("-c", "--columns",
              "Number of characters before line wrap") do |cols|
        @settings[:columns] = cols
      end
      opts.on("-t", "--tabs", "Number of spaces to use for tabs") do |tab|
        @settings[:tab] = tab
      end
      opts.on("-l", "--load", "Load secondary run control file") do |rc|
        @settings[:rc] = rc
        @settings[:load_secondary_rc] = true
      end
      opts.on("-m", "--manifest", "Load alternate manifest") do |manifest|
        @settings[:manifest] = manifest
      end
    end.parse!
  end

  # Load the run control file and merge it with @settings.
  # Some of the possible entries in the settings hash don't
  # make much sense in an rc file, but I don't see much
  # point in writing exceptions for them.
  def self.loadrc
    begin
      rc = Psych::load(File::Open(@settings[:rc]).read())
    rescue Errno::ENOENT
      unless @settings[:quiet]
        puts "Could not open run control file"
      end
    end
    @settings.merge!(rc)
  end

  # Load the run control file and the settings hash.  This is
  # just a nicety to make the code in the main body of the
  # program a little cleaner.
  def self.collect_settings
    self.loadrc
    self.parse_args

    if @settings[:load_secondary_rc]
      self.loadrc
    end
  end
end
