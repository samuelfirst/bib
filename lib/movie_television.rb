# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

require 'net/http'
require 'rexml/document'
require 'json'

# Pull info on movies and tv shows from IMDB's database.  Unforunately,
# IMDB doesn't provide an official API, so we have to make due with
# scraping the data from it's web-interface (thankfully, they use json
# internally, which makes it a bit easier to scrape the data out).
class IMDB
  # Builds query, scrapes page, and returns array
  # containing results.
  #
  # The search feature uses a url format that's something
  # like this: https://www.imdb.com/find?ref_=nv_sr_fn&q=12+angry+men&s=all.
  #
  # 
  def self.query_database(query)
    # Build the query, and download the page.
    url = URI("https://www.imdb.com/find?ref_=nv_sr_fn&q=
    #{query.gsub(' ', '+')}&s=all")
    puts url
    page = Net::HTTP.get(url)

    # Scrape the page for results
    data = REXML::Document.new page
  end

  def self.scrape_data(page)
  end
end
