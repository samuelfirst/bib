# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

require 'net/http'
require 'rexml/document'

# This class contains methods for scraping articles for information.
# It will attempt to search for relevant meta-tags, and if that fails,
# it will fall back to looking for the elements in the page.
class WebArticle
  # Scrape page for any names, return first as string.  The name of the
  # author will almost always be behind 'By', so it searches for that.
  # If it can't find a name, it will return an empty string.
  #
  # Example:
  #   my_author = WebArticle::search_author('<here be lots of html>')
  def self.search_author(page)
  end

  # Scrape page for title, return as string.  This function will search
  # for the title tag, and attempt to pull the title from there.  Most
  # sites will have the title in the format <article title - site>, so
  # before returning the title will need to be split on '-' and stripped
  # (title.split('-')[0].strip).
  # Example:
  #   my_title = WebArticle::search_title('<here be lots of html>')
  def self.search_title(page)
  end

  # Scrape page for any dates, return first as string.  Dates come in
  # many formats unfortunately, which means that this function will
  # probably return lots of false positives.  It expects the date to
  # be some combination of month, day, year with some sort of seperator,
  # so formats like the one used by the USAF will not be supported.
  # Example:
  #   my_date = WebArticle::search_date('<here be lots of html>')
  def self.search_date(page)
  end

  # Scrape page for publisher, return as string.  Looks first for copyright
  # notice, then, if it can't find one, it pulls the www. and extension off
  # of the name of the site and returns that.
  # Example:
  #   my_publisher = WebArticle::search_publisher('<here be lots of html>')
  def self.search_publisher(page, address)
  end
  
  # Takes string containing page address.
  # Returns dictionary containing author, title, date published, site,
  # publisher.
  #
  # Example:
  #   >>> WebArticle::scrape_page(https://www.bbc.co.uk/news/science-environment-47873592)
  #   #=> {:author => "https://www.facebook.com/bbcnews",
  #        :title => "First ever black hole image released",
  #        :date => 10 April 2019,
  #        :site => https://www.bbc.co.uk/news/science-environment-47873592,
  #        :publisher => bbc}
  def self.scrape_page(page)
    # Perform sanity checks on page
    if page.class != String
      raise ArgumentError, 'Invalid Argument: must be a string'
    elsif page !~ /(http?:\/\/)?.*\.?.*\..*/
      raise ArgumentError, 'Invalid Argument: must be web address'
    end

    # Download page and attempt to scrape meta tags
    # meta tags can be found at https://wiki.whatwg.org/wiki/MetaExtensions
    # and https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta.
    data = REXML::Document.new Net::HTTP.get(URI(page))

    # Tags for each category
    author_tags = ['author',
                   'creator',
                   'citation_author',
                   'dc.creator',
                   'shareaholic:article_author_name',
                   'twitter:creator',
                   'article:author']
    title_tags = ['citation_title',
                  'da_pageTitle',
                  'dc.title',
                  'dcterms.title',
                  'twitter:title',
                  'og:title']
    date_tags = ['date',
                 'citation_publication_date',
                 'dc.created',
                 'dc.date.issued',
                 'dc.dateSubmitted',
                 'dcterms.created',
                 'dcterms.date',
                 'dcterms.dateAccepted',
                 'dcterms.dateSubmitted',
                 'dcterms.issued',
                 'icas.datetime',
                 'shareaholic:article_published_time',
                 'created',
                 'FSDateCreation',
                 'FSDatePublish']
    pub_tags = ['publisher',
                'citation_journal_title',
                'citation_publisher',
                'citation_technical_report_institution',
                'dc.publisher',
                'dcterms.publisher']

    # Hash with info to be returned
    info = {:author => '',
            :title => '',
            :date => '',
            :site => page,
            :publisher => ''}

    # loop through meta tags, searching for relevant ones.
    data.root.elements['head'].elements.each("meta") do |meta|
      if meta["name"].member? author_tags || meta["property"].member? author_tags
        info[:author] = meta["content"]
      elsif meta["name"].member? title_tags || meta["property"].member? title_tags
        info[:title] = meta["content"]
      elsif meta["name"].member? date_tags || meta["property"].member? date_tags
        info[:date] = meta["content"]
      elsif meta["name"].member? pub_tags || meta["property"].member? pub_tags
        info[:publisher] = meta["content"]
      end
    end

    # If any information was not found, attempt to scrape it from the
    # content of the page.
    if info[:author].empty?
      info[:author] = self.search_author(data)
    end
    if info[:title].empty?
      info[:title] = self.search_title(data)
    end
    if info[:date].empty?
      info[:date] = self.search_date(data)
    end
    if info[:publisher].empty?
      info[:publisher] = self.search_publisher(data, page)
    end

    return info
  end
end
