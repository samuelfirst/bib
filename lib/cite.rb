# Copyright (C) 2019 Samuel First
# This file is part of bib.

#     bib is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     bib is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with bib.  If not, see <https://www.gnu.org/licenses/>.

require 'psych'
require 'error'
require 'settings'

# Parse Citation Templates to build citations. Info on the template
# standard can be found in the file Template_Standard.md.
#
# Functions marked with \* are meant for internal use with the TemplateParser
# class only.
class TemplateParser
  # Returns a name in the order of [last name], [first name] [middle initial]
  #
  # Arguments:
  # * name: string, name to reorder.
  # Example:
  #   TemplateParser::last_name_first('why the lucky stiff')
  #   #=> "stiff, why the lucky"
  # \*
  def self.last_name_first(name)
    split_name = name.split(' ')
    return "#{split_name[-1]}, #{split_name[0..-2].join(' ')}"
  end

  # Returns a name as a string in the order of [first initial].
  # \[middle initial\]. \[last name\]
  #
  # Arguments:
  # * name: string, name to convert.
  # Example:
  #   TemplateParser::initials('why the lucky stiff')
  #   #=> "w. t. l. stiff"
  # \*
  def self.initials(name)
    split_name = name.split(' ')
    split_name[0...-1].each {|name| name.replace("#{name[0]}.")}
    return split_name.join(' ')
  end

  # Returns an array containing each line of the string passed to it wrapped
  # at n characters.
  #
  # Arguments:
  # * line: string, line to wrap.
  # * limit: int, number of characters to wrap line at.
  # Example:
  #   TemplateParser::line_wrap(<long string of a's>, 20)
  #   #=> ["aaaaaaaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaa",
  #        "aaaaaaaaaaaaaaaaaaaa"]
  # \*
  def self.line_wrap(line, limit)
    wrapped_line = []
    (1..(line.length / limit)+1).each do |index|
      wrapped_line.append(line[(index-1)*limit...index*limit])
    end
    return wrapped_line
  end

  # Returns a string of a line wrapped at n characters with every line after
  # the first tabbed over x spaces.
  #
  # Arguments:
  # * line: string, line to wrap.
  # * limit: integer, number of characters to wrap line at.
  # * tab_size: number of spaces to use when tabbing lines over.
  # Example:
  #   puts TemplateParser::tabbed_line_wrap(<long string of a's>, 20, 2)
  #   #=> aaaaaaaaaaaaaaaaaaaa
  #         aaaaaaaaaaaaaaaaaa
  #         aaaaaaaaaaaaaaaaaa
  # \*
  def self.tabbed_line_wrap(line, limit, tab_size)
    wrapped_line = self.line_wrap(line, limit - tab_size)
    (1...wrapped_line.length).each do |num|
      wrapped_line[num - 1] << wrapped_line[num][0...tab_size]
      wrapped_line[num] = " "*tab_size << wrapped_line[num][tab_size..]
    end
    return wrapped_line.join("\n")
  end
  
  # Tokenize the template string.
  #
  # Arguments:
  # * template_string: string, string containing the actual template.
  #
  # Returns:
  # * Array containing each token.
  #
  # Each token is contained in curly brackets, so we just need
  # to read through the string and put each char after an unescaped
  # opening bracket into a temporary variable.  Then when we
  # encounter a closing bracket, we push the temporary variable onto
  # the bottom of the queue.
  #
  # Some tokens need to be used more than once, or need to be included
  # regardless of whether or not there is relevant information in the
  # token.  For these we set special flags telling the parser how to
  # interpret them.  See the list below for special flags, and how
  # they should be interpreted.
  # * repeated: token should be applied multiple times
  # * generic: token should be included even if no data is present
  #
  # Once we're done tokenizing the string, we return the queue.  The
  # queue should be read in a FIFO (First in First Out) order.
  #
  # This function is only meant to be used internally by the parser,
  # and should not be called anywhere else.
  #
  # Example:
  #   TemplateParser::tokenize('{AUTHOR.}{TITLE.}{CITY:}{PUBLISHER,}{YEAR.}')
  #   #=> [{"string"=>"AUTHOR."},
  #        {"string"=>"TITLE."},
  #        {"string"=>"CITY:"},
  #        {"string"=>"PUBLISHER,"},
  #        {"string"=>"YEAR."}]
  # \*
  def self.tokenize(template_string)
    queue = []
    token = {"string" => ''}
    interpret_literally = false
    inside_token = false
    
    template_string.each_char do |char|
      # Previous char was an escape character ('\'), so this char
      # should be interpreted as part of the string.
      if interpret_literally
        if char == 'n'
          token["string"] << "\n"
        else
          token["string"] << char
        end
        interpret_literally = false
      # If the char is an escape character, set interpret_literally
      # to true.
      elsif char == "\\"
        interpret_literally = true
      # Open new token by blanking out token variable.
      elsif char == "{"
        if !inside_token
          if !token["string"].empty?
            queue.append(token)
          end
          token = {"string" => ''}
          inside_token = true
        else
          Error::throw("Template", "Invalid '{', Token already open")
        end
      # Close current token and push it to the queue.
      elsif char == "}"
        if !inside_token
          Error::throw("Template", "invalid '}', Token not open")
        end
        queue.append(token)
        token = {"string" => ''}
        inside_token = false
      # Set repeated flag for previous token
      elsif char == "*" && !inside_token
        queue[-1]["repeated"] = true
      # Set flag for generic token and push char to token
      elsif !inside_token
        token["generic"] = true
        token["string"] << char
      # Not a special character; append it to the token.
      else
        token["string"] << char
      end
    end

    # Push token to queue if it has not been pushed
    # This will misbehave if the last two tokens are
    # the same.  I'm working on a more elegant solution,
    # so hopefull this is only temporary.
    if token != queue[-1]
      queue.append(token)
    end

    return queue
  end

  # Parse the template file.
  #
  # Arguments:
  # * template_file: The path to the template file.
  # * type: The type of template to use.
  # * info: Dictionary containing citation info.
  #
  # Returns:
  # * String containing the properly formatted citation
  #
  # We start by pulling out the meta variables like INITIALS or LNF
  # that we use for transformations of our data.
  #
  # Then we try to pull out the template string for our type of work.
  # If there isn't one available, we attempt to fall back to the generic
  # template string. If that isn't available, we throw an error and bail
  # out.
  #
  # Once we have our template string, we pass it to the tokenizer to be
  # converted to a queue, which we iterate through, attempting to replace
  # any token variables with their assosciated value.  If the token does
  # not have an assosciated value, we just skip over it.  If the token has
  # not been discarded, we append it to the end of our citation.
  #
  # Some tokens will have special flags that tell us to repeat them or to
  # include them even if we don't have an assosciated value for them.
  #
  # Finally, we attempt to pull out the meta variables like TLW, EOL, etc.
  # and use them to figure out which transformations to apply to our
  # citation.  Then, after applying the transformations, we return the
  # citation as a string.
  def self.parse(template_file, type, info)
    # Load template file do the initial parsing with Psych
    template = Psych::load(File::open(template_file).read())

    # Pull out meta variables for transformations of data
    if template["meta"]["INITIALS"]
      info["AUTHOR"] = self.initials(info["AUTHOR"])
    end
    if template["meta"]["LNF"]
      info["AUTHOR"] = self.last_name_first(info["AUTHOR"])
    end

    # Pull out and tokenize the relevant template string or raise an error
    # if there isn't one.
    if template["template"][type]
      queue = self.tokenize(template["template"][type])
    elsif template["template"]["generic"]
      queue = self.tokenize(template["template"]["generic"])
    else
      Error::throw("Template", "Template String #{type} Not Found")
    end

    # Go through the tokens and replace them with the relevant info if there
    # is any.  Otherwise, skip over the token.
    citation = ''
    queue.each do |token|
      # If the token is generic, just append it to the citation
      if token["generic"]
        citation << token["string"]
        next
      end
      info.each_key do |key|
        if token["string"].include? key
          chunk = ''
          index = token["string"].index(key)

          # If the token is repeated, run through each entry and
          # append it to the citation
          if token["repeated"] && info[key].class == Array
            info[key].each do |item|
              if index == 0
                chunk << item.to_s << token["string"][key.length..]
              else
                chunk << token["string"][0..index] <<
                  item.to_s << token["string"][index+key.length..]
              end
            end
            
          # If the token is not repeated, just append it to the citation
          else
            if index == 0
              chunk << info[key].to_s << token["string"][key.length..]
            else
              chunk << token["string"][0...index] <<
                info[key].to_s << token["string"][index+key.length..]
            end
          end

          citation << chunk
          break
        end
      end
    end

    # Apply final transformations to the citation
    # If the final character is punctuation, replace it with the
    # string in EOC
    if template["meta"]["EOC"]
      if ".,:(){}[]".include? citation[-1]
        citation = citation[0...-1] << template["meta"]["EOC"]
      end
    end
    # Line wrap the citation
    if template["meta"]["LW"]
      citation = self.line_wrap(citation, 79).join("\n")
    end
    # Line wrap the citation, but with every line after the
    # first line tabbed over
    if template["meta"]["TLW"]
      citation = self.tabbed_line_wrap(citation,
                                       Settings::settings[:columns],
                                       Settings::settings[:tab])
    end
    # Stick the character in EOL at the end of each line
    if template["meta"]["EOL"]
      split_citation = citation.split("\n")
      split_citation[0...-1].each do |line|
        line.replace("#{line}#{template['meta']['EOL']}")
      end
      citation = split_citation.join("\n")
    end
    # Remove duplicated punctuation from the citation
    if template["meta"]["RDP"]
      cite = ''
      prev = ''
      citation.each_char do |char|
        if char == prev and ".,:(){}[]".include? char
          next
        else
          cite << char
          prev = char
        end
      end
      citation = cite
    end

    # Return the citation as a string
    return citation
  end
end
