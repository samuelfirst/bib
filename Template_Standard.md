# Citation Template Standard
This standard defines the format used to create citation templates for bib.
The format is built on top of YAML, which it uses to define sections.

## Parts of the Template

### Meta

The meta block contains information about general formatting that should be
applied to the template.  For example, whether names should be listed last
name first, or whether lines should be tabbed over after being wrapped.
Listed below are the available meta-variables, with their type and purpose.

| Variable | Type    | Purpose                                               |
|:---------|:--------|:------------------------------------------------------|
| EOL      | String  | Character to put at the end of each line              |
| EOC      | String  | Character to put at the end of the citation           |
| INITIALS | Boolean | Use author's first and middle initials, not full name |
| LNF      | Boolean | Put the author's name in the last name first order    |
| TLW      | Boolean | Tab every line after the first line when wrapping     |
| RDP      | Boolean | Remove Duplicated Punctuation                         |

#### Example

```
meta:
  INITIALS: true
  LNF: true
  TLW: true
template:
  ...
```

### Template

The template block contains subsections defining the templates to use for
different types of work.  Below is a list of available template types.  It is
not necessary for a template block to contain all of the types listed,
however, it is recommended to at least include a generic template.

| Template               | Purpose                                              |
|:-----------------------|:-----------------------------------------------------|
| generic                | If no other template fits, this template is used.    |
| book                   | Citation for a whole book                            |
| chapter                | Chapter or part of a book                            |
| translated_book        | A book that has been translated                      |
| edited_book            | A book that has been edited, but has no author       |
| edited_book_author     | A book that has been edited, and has an author       |
| e_book                 | An electronic book                                   |
| book_review            | A review of a book                                   |
| journal_article        | An article from a journal                            |
| article                | A news or magazine article                           |
| interview              | An interview                                         |
| masters_thesis         | A masters thesis                                     |
| phd_thesis             | A phd thesis                                         |
| website                | A website                                            |
| social_media           | Content shared through social media                  |
| personal_communication | Content shared via email, text mesasges, calls, etc. |
| booklet                | A printed/bound work with no named publisher         |
| conference_proceedings | Conference Proceedings                               |
| in_proceedings         | An article published in conference proceedings       |
| collection             | A book that is part of a larger collection           |
| manual                 | Technical Documentation                              |
| tech_report            | Report published by academic institution             |
| unpublished            | An unpublished document with an author and title     |
| television_episode     | An episode of a television show                      |
| television_show        | A television show                                    |
| motion_picture         | A movie                                              |
| song                   | A song                                               |

#### Example

```
meta:
  ...
template:
  generic:
    ...
  book:
	...
  chapter:
	...
  ...
```

Each template is made up of a series of tokens surrounded by single quotes
(`'`).  Tokens are defined as anything between curly brackets (`{ and }`).
Each part of a citation is inserted into a token, like this: `'{AUTHOR,
}{"TITLE". }...'`.  Each token contains a variable defining what information
the token should contain (you can find a list of these below) as well as any
punctuation or other formatting that should be inserted around the
information.  When the interpreter runs over the template, it will transform
it into a properly formatted citation with the information passed to it.  For
example, the above template would be translated to: `My Author, "Work
Title". ...`.  If a template must contain a curly bracket or single quote as
part of the citation, it can be escaped with a backslash, like this:
`\{ or \} or \'`. If the citation must contain a backslash, it can be escaped
like this: `\\`.

| Variable      | Purpose                                             |
|:--------------|:----------------------------------------------------|
| AUTHOR        | Author of the work                                  |
| AUTHORS       | Additional Authors                                  |
| EDITOR        | Editor of an editied work                           |
| TRANSLATOR    | Translator of a translated work                     |
| DIRECTOR      | Director of a movie or television show              |
| PRODUCER      | Producer of a movie or television show              |
| ARTIST        | Artist that recorded song                           |
| TITLE         | Title of the work                                   |
| CHAPTER       | Title of the cited chapter                          |
| EPISODE       | Episode of a television show                        |
| BOOK          | Title of the work cited in a review                 |
| ALBUM         | Title of the album a musical work is from           |
| REC_MEDIUM    | Medium a musical work was recorded on               |
| PAGE_NUMBER   | The number of the page being referenced             |
| EDITION       | Edition of the work                                 |
| DAY           | Day the work was published                          |
| MONTH         | Month the work was published                        |
| YEAR          | Year the work was published                         |
| ORIGINAL_YEAR | Year the work was originally published              |
| PUBLISHER     | The publisher of the work                           |
| EDITION       | Edition of the work                                 |
| VOLUME        | Volume of the work                                  |
| SERIES        | Series the work belongs to                          |
| ADDRESS       | Address of the publisher                            |
| CITY          | City the publisher is based out of                  |
| STATE         | State the publisher is based out of                 |
| COUNTRY       | Country the publisher is based out of               |
| ANNOTATION    | An annotation                                       |
| CROSSREF      | Database key of the entry being cross referenced    |
| URL           | Url where the resource can be found                 |
| INSTITUTION   | Sponsoring institution of a technical report        |
| SCHOOL        | The name of the school where a thesis was written   |
| TYPE          | The type of technical report                        |
| KEY           | Used by BibTeX when author info is missing          |
| NOTE          | Any additional information that can help the reader |
| JOURNAL       | The name of the journal a work was published in     |
| COLLECTION    | The name of the collection a work was published in  |
| PROCEEDINGS   | The name of the conference proceedings              |
| DOI           | Digital Object Identifier for the work              |

#### Example

```
meta:
  ...
template:
  generic:
    '{AUTHOR.}{TITLE.}{CITY:}{PUBLISHER,}{YEAR.}'
  ...
```

Sometimes a token will need to be repeated across multiple values, for
example, in a work with multiple authors.  In these cases, the token must be
followed by an asterisk (`*`), like this: `{TOKEN}*`.  Similarly to curly
brackets an asterisk can be escaped with a backslash: `\*`.

#### Example

```
meta:
  ...
template:
  generic:
    '{AUTHOR.}{AUTHORS,}*{TITLE.}{CITY:}{PUBLISHER,}{YEAR.}'
  ...
```

If a template must contain a string that is not part of any token, that string
may be placed between tokens, like this: `{TOKEN_1}my_string{TOKEN_2}`, and it
will be included in the final citation between those two tokens.

#### Example

```
meta:
  ...
template:
  generic:
    'My Citation: {AUTHOR.}{TITLE.}{CITY:}{PUBLISHER,}{YEAR.}'
  ...
```

### Comments

Comments are lines that are ignored by the parser.  They begin with the pound
(#) character.

#### Example

```
# This is the template for MHRA Style Formatting.  Documentation on the
# MHRA format can be found here: http://www.mhra.org.uk/style
meta:
  ...
template:
  ...
```

## Writing Templates

### The Basics

### Leveraging YAML
Because the language is built on top of YAML, it can leverage
