# The CORRECT Citation Standard

The **C**omputer **OR**iented **RE**adable **C**itation **T**ext, or CORRECT,
standard is a standard of citation that aims to create citations that are easy
to read and write for both humans and machines.

## The Standard

Information is listed in the following order: *author(s), title, chapter title,
edition, volume, date, publisher, location (physical or web address), extra
information*.  Each item is separated with a pipe character ('|').  If the item
contains the pipe character, it is escaped with a backslash ('\\|').  If an
item is not known, its space is left blank, however the pipe is still
inserted.  If the citation spans multiple lines, the end of the line is
prefixed with a backslash.

For Example:

```
J. Random Author|The History of Unix and the \| character||||2015|O'Really|\
55 Publisher Drive Lopotsabes, CA 75025|
```

## Advantages of the Standard

* It's small enough that it can be remembered easily.  The standard is so
  small in fact that if you want to write it down, you can do so basically
  anywhere.  Here's some ideas of places that you could put the standard:
  * A sticky note
  * A post card
  * A reciept
  * A napkin (preferably unused)
  * The back of the Chicago Manual of Style
* It's easier for computers to understand than other standards.
* It can be represented entirely in plain text, without special formatting
  like *italics* or **bold-face**.
* Unlike some other citation formats, which seem to change with the weather
  (*cough* MLA *cough*), the CORRECT standard will **never** change.
  
## Disadvantages of the Standard

* It isn't as widely accepted as other standards.

## Where can the Standard be Used?

Anywhere.  The CORRECT standard could be used in place of any other standard
of citation.

## Citing This Document

```
Samuel First|The CORRECT Citation Standard||||4/17/2019||\
https://www.gitlab.com/samuelfirst/bib/blob/master/The_CORRECT_Citation_Standard.md|
```
